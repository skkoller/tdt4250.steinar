package tdt4250.steinar.courseModeling;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;

import tdt4250.steinar.courseModeling.util.CoursemodelResourceFactoryImpl;







public class CoursePageGenerator {
	
	public static void main(String[] args) throws IOException {
		String input;
		String output;
		if (args.length == 2) {
			input = args[0];
			output = args[1];
		}
		else if (args.length == 2) {
			input = args[0];
			output = "model/CoursePage";
		}
		else { 
			input = "model/Department.xmi";
			output = "model/CoursePage";
		}
		//Argument need to be department.xmi
		Department department = getDepartment(input);
		CoursePageHTMLGenerator generator = new CoursePageHTMLGenerator();
		generator.generateHTML(department, output);
		System.out.print(department.getStudyprograms().get(0).getCourse().get(0).getCourseName());
	}

	public static Department getDepartment(String args) throws IOException {
		ResourceSet resSet = new ResourceSetImpl();
		resSet.getPackageRegistry().put(CourseModelingPackage.eNS_URI, CourseModelingPackage.eINSTANCE);
		resSet.getResourceFactoryRegistry().getExtensionToFactoryMap().put("*", new CoursemodelResourceFactoryImpl());
		Resource resource = resSet.getResource(URI.createFileURI(args), true);
		for (EObject eObject : resource.getContents()) {
			if (eObject instanceof Department) {
				return (Department) eObject;
			}
		}
		return null;
	}
	
	public static String generateHtml(Department newDepartment, String output) throws FileNotFoundException {
		CoursePageHTMLGenerator generator = new CoursePageHTMLGenerator();
		generator.generateHTML(newDepartment, output);
		return null;
	}
}

