package tdt4250.steinar.courseModeling;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class CoursePageHTMLGenerator {

	ArrayList<String> generateHTML(Department department, String output) throws FileNotFoundException{
		ArrayList<String> htmlPages = new ArrayList<String>();
		for(Course course : department.getStudyprograms().get(0).getCourse()) {
			if(course.getCourseinstance().size() > 0) {
				htmlPages.add(generateCourseHTML(course));
			}
		}
		//saves each course to its own .html file in the model folder
		for(int i = 0; i < htmlPages.size(); i++) {
			PrintWriter out = new PrintWriter(output + i + ".html");
			out.println(htmlPages.get(i));
			out.close();
		}
		return htmlPages;
	}
	
	String generateCourseHTML(Course course) {
		String coursehtml = "<html>"
				+ "<title>"
				+ course.getCourseCode()
				+ " "
				+ course.getCourseName()
				+ "</title>"
				+ "<body>"
				+ "<h1>"
				+ course.getCourseCode()
				+ " "
				+ course.getCourseName()
				+ "</h1>"
				+ "<br>"
				+ courseWorkTableGen(course)
				+ contentGen(course)
				+ courseWorkerGen(course)
				+ pkGen(course)
				+ reductionTableGen(course)
				+ timetableGen(course);

		coursehtml += "</body></html>";
		
		return coursehtml;
	}
	
	String courseWorkTableGen(Course course) {
		Evaluations evaluation = course.getCourseinstance().get(0).getEvaluations();
		String evalTable = "<div><h1>Examination Arrangements</h1><table border=\"1\">"
							+"<tr>"
							+ "<th>"
							+ "Evaluation form"
							+ "</th>"
							+ "<th>"
							+ "Weighting"
							+ " </th>"
							+ "</tr>";
		for (EvaluationObject eval : evaluation.getEvaluationobject()) {
			evalTable += "<tr>"
					+ "<th>"
					+ eval.getEvaluationsForm()
					+ "</th>"
					+ "<th>"
					+ eval.getCredits()
					+ "</th>"
					+ "</tr>";
		}
		evalTable += "</table></div>";
		return evalTable;
	}
	
	String contentGen(Course course) {
		String contentDiv = "<div><h1>Course Content</h1>"
							+ course.getCourseContent()
							+ "</div>";
		return contentDiv;
	}
	
	String courseWorkerGen(Course course) {
		String lectureDiv = "<div><h1>Lecture</h1>";
		for(CourseWorker worker : course.getCourseinstance().get(0).getCourseworker()) {
			if(worker.getCourseRole() == personRoleType.getByName("Lecture")) {
				lectureDiv += "<br>"
								+ worker.getEmployee().getFirstName()+" "+worker.getEmployee().getSurName()
								+ "</br>";
			}
		}
		lectureDiv += "</div>";
		
		lectureDiv += "<div><h1>Course cordinator</h1>";
		for(CourseWorker worker : course.getCourseinstance().get(0).getCourseworker()) {
			if(worker.getCourseRole() == personRoleType.getByName("CourseCordinator")) {
				lectureDiv += "<br>"
								+ worker.getEmployee().getFirstName()+" "+worker.getEmployee().getSurName()
								+ "</br>";
			}
		}
				lectureDiv += "</div>";
		return lectureDiv;
	}
	
	String pkGen(Course course) {
		String pkDiv = "<div><h1>Recommended Previous Knowledge</h1>";
		for(Precondition preCourse : course.getPrecondition()) {
			if(preCourse.getPreconditionStatus()==PrecondistionType.getByName("Recommended")) {
				pkDiv += "Course"
					  + preCourse.getCourse().getCourseCode()
					  + " "
					  + preCourse.getCourse().getCourseName()
					  + ", or equivalent. <br>";
			}
		}
		pkDiv += "</div>";
		
		pkDiv += "<div><h1>Required Previous Knowledge</h1>";
		for(Precondition preCourse : course.getPrecondition()) {
			if(preCourse.getPreconditionStatus()==PrecondistionType.getByName("Required")) {
			pkDiv += "Course"
				  + preCourse.getCourse().getCourseCode()
				  + " "
				  + preCourse.getCourse().getCourseName()
				  + ", or equivalent. <br>";
			}
		}
		pkDiv += "</div>";
		return pkDiv;
	}

	String reductionTableGen(Course course) {
		String reducDiv = "<div><h1>Credit Reductions</h1><br><table border=\"1\">"
				+"<tr>"
				+ "<th>"
				+ "Course Code"
				+ "</th>"
				+ "<th>"
				+ "Reduction"
				+ " </th>"
				+ "</tr>";
		for (Reduction reduction : course.getReduction()) {
			String redValueString = String.valueOf(reduction.getCreditReduction()); 
			reducDiv += "<tr>"
					+ "<th>"
					+ reduction.getCourse().getCourseCode()
					+ "</th>"
					+ "<th>"
					+ redValueString
					+ " </th>"
					+ "</tr>";

		}
		reducDiv += "</table></div>";
		return reducDiv;
	}
	
	@SuppressWarnings("deprecation")
	String timetableGen(Course course) {
		String timetableDiv = "<div><h1>Timetable</h1><table border=\"1\"><br>"
							+ "<tr>"
							+ "<th>"
							+ "Day"
							+ "</th>"
							+ "<th>"
							+ "Time"
							+ "</th>"
							+ "<th>"
							+ "Type"
							+ "</th>"
							+ "<th>"
							+ "Room"
							+ "</th>"
							+ "</tr>";
		for(CourseWorkObject courseWork : course.getCourseinstance().get(0).getCoursework().getCourseworkobject()) {
			   timetableDiv += "<tr>"
							+ "<th>"
							+ courseWork.getDay()
							+ "</th>"
							+ "<th>"
							//+ courseWork.getStart().get+":"+courseWork.getStart().getMinutes()+" - "
							//+ courseWork.getEnd().getHours()+":"+courseWork.getEnd().getMinutes()
							+ "</th>"
							+ "<th>"
			   				+ courseWork.getCourseWorkType()
			   				+ "</th>"
							+ "<th>"
							+ courseWork.getRoom()
							+ "</th>"
							+ "</tr>";
		}
		timetableDiv += "</table></div>";
		return timetableDiv;
	}
}

