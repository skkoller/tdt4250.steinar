/**
 */
package tdt4250.steinar.courseModeling;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getDepartmentName <em>Department Name</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getPhoneNummber <em>Phone Nummber</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getEmail <em>Email</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getEmployee <em>Employee</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Department#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment()
 * @model
 * @generated
 */
public interface Department extends EObject {
	/**
	 * Returns the value of the '<em><b>Department Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Department Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Department Name</em>' attribute.
	 * @see #setDepartmentName(String)
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_DepartmentName()
	 * @model
	 * @generated
	 */
	String getDepartmentName();

	/**
	 * Sets the value of the '{@link tdt4250.steinar.courseModeling.Department#getDepartmentName <em>Department Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Department Name</em>' attribute.
	 * @see #getDepartmentName()
	 * @generated
	 */
	void setDepartmentName(String value);

	/**
	 * Returns the value of the '<em><b>Phone Nummber</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Phone Nummber</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Phone Nummber</em>' attribute.
	 * @see #setPhoneNummber(String)
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_PhoneNummber()
	 * @model
	 * @generated
	 */
	String getPhoneNummber();

	/**
	 * Sets the value of the '{@link tdt4250.steinar.courseModeling.Department#getPhoneNummber <em>Phone Nummber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Phone Nummber</em>' attribute.
	 * @see #getPhoneNummber()
	 * @generated
	 */
	void setPhoneNummber(String value);

	/**
	 * Returns the value of the '<em><b>Email</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Email</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Email</em>' attribute.
	 * @see #setEmail(String)
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_Email()
	 * @model
	 * @generated
	 */
	String getEmail();

	/**
	 * Sets the value of the '{@link tdt4250.steinar.courseModeling.Department#getEmail <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Email</em>' attribute.
	 * @see #getEmail()
	 * @generated
	 */
	void setEmail(String value);

	/**
	 * Returns the value of the '<em><b>Employee</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.steinar.courseModeling.Employee}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Employee</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Employee</em>' containment reference list.
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_Employee()
	 * @model containment="true"
	 * @generated
	 */
	EList<Employee> getEmployee();

	/**
	 * Returns the value of the '<em><b>Studyprograms</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.steinar.courseModeling.StudyPrograms}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprograms</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprograms</em>' containment reference list.
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_Studyprograms()
	 * @model containment="true"
	 * @generated
	 */
	EList<StudyPrograms> getStudyprograms();

	/**
	 * Returns the value of the '<em><b>Course</b></em>' containment reference list.
	 * The list contents are of type {@link tdt4250.steinar.courseModeling.Course}.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Course</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Course</em>' containment reference list.
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getDepartment_Course()
	 * @model containment="true"
	 * @generated
	 */
	EList<Course> getCourse();

} // Department
