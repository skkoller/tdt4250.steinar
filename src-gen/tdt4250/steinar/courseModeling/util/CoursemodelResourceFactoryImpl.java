package tdt4250.steinar.courseModeling.util;

import org.eclipse.emf.common.util.URI;

import org.eclipse.emf.ecore.resource.Resource;

import org.eclipse.emf.ecore.resource.impl.ResourceFactoryImpl;


public class CoursemodelResourceFactoryImpl extends ResourceFactoryImpl {

	
	public CoursemodelResourceFactoryImpl() {
		super();
	}
	
	
	@Override
	public Resource createResource(URI uri) {
		Resource result = new CoursemodelResourceImpl(uri);
		return result;

	}
}

