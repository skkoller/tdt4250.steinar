/**
 */
package tdt4250.steinar.courseModeling;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Student</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.steinar.courseModeling.Student#getStudentID <em>Student ID</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.Student#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getStudent()
 * @model
 * @generated
 */
public interface Student extends Person {
	/**
	 * Returns the value of the '<em><b>Student ID</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Student ID</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Student ID</em>' attribute.
	 * @see #setStudentID(String)
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getStudent_StudentID()
	 * @model
	 * @generated
	 */
	String getStudentID();

	/**
	 * Sets the value of the '{@link tdt4250.steinar.courseModeling.Student#getStudentID <em>Student ID</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Student ID</em>' attribute.
	 * @see #getStudentID()
	 * @generated
	 */
	void setStudentID(String value);

	/**
	 * Returns the value of the '<em><b>Studyprograms</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link tdt4250.steinar.courseModeling.StudyPrograms#getStudent <em>Student</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Studyprograms</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Studyprograms</em>' reference.
	 * @see #setStudyprograms(StudyPrograms)
	 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getStudent_Studyprograms()
	 * @see tdt4250.steinar.courseModeling.StudyPrograms#getStudent
	 * @model opposite="student" required="true"
	 * @generated
	 */
	StudyPrograms getStudyprograms();

	/**
	 * Sets the value of the '{@link tdt4250.steinar.courseModeling.Student#getStudyprograms <em>Studyprograms</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Studyprograms</em>' reference.
	 * @see #getStudyprograms()
	 * @generated
	 */
	void setStudyprograms(StudyPrograms value);

} // Student
