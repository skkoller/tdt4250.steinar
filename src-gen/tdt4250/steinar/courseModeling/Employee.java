/**
 */
package tdt4250.steinar.courseModeling;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Employee</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see tdt4250.steinar.courseModeling.CourseModelingPackage#getEmployee()
 * @model
 * @generated
 */
public interface Employee extends Person {

} // Employee
