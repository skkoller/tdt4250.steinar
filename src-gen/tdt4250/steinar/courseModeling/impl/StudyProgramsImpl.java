/**
 */
package tdt4250.steinar.courseModeling.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.steinar.courseModeling.Course;
import tdt4250.steinar.courseModeling.CourseModelingPackage;
import tdt4250.steinar.courseModeling.Student;
import tdt4250.steinar.courseModeling.StudyPrograms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Study Programs</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.StudyProgramsImpl#getStudyProgramCode <em>Study Program Code</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.StudyProgramsImpl#getStudyProgramName <em>Study Program Name</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.StudyProgramsImpl#getStudent <em>Student</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.StudyProgramsImpl#getCourse <em>Course</em>}</li>
 * </ul>
 *
 * @generated
 */
public class StudyProgramsImpl extends MinimalEObjectImpl.Container implements StudyPrograms {
	/**
	 * The default value of the '{@link #getStudyProgramCode() <em>Study Program Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgramCode()
	 * @generated
	 * @ordered
	 */
	protected static final String STUDY_PROGRAM_CODE_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStudyProgramCode() <em>Study Program Code</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgramCode()
	 * @generated
	 * @ordered
	 */
	protected String studyProgramCode = STUDY_PROGRAM_CODE_EDEFAULT;

	/**
	 * The default value of the '{@link #getStudyProgramName() <em>Study Program Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgramName()
	 * @generated
	 * @ordered
	 */
	protected static final String STUDY_PROGRAM_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getStudyProgramName() <em>Study Program Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyProgramName()
	 * @generated
	 * @ordered
	 */
	protected String studyProgramName = STUDY_PROGRAM_NAME_EDEFAULT;

	/**
	 * The cached value of the '{@link #getStudent() <em>Student</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudent()
	 * @generated
	 * @ordered
	 */
	protected EList<Student> student;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> course;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected StudyProgramsImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseModelingPackage.Literals.STUDY_PROGRAMS;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStudyProgramCode() {
		return studyProgramCode;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStudyProgramCode(String newStudyProgramCode) {
		String oldStudyProgramCode = studyProgramCode;
		studyProgramCode = newStudyProgramCode;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_CODE, oldStudyProgramCode, studyProgramCode));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getStudyProgramName() {
		return studyProgramName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setStudyProgramName(String newStudyProgramName) {
		String oldStudyProgramName = studyProgramName;
		studyProgramName = newStudyProgramName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_NAME, oldStudyProgramName, studyProgramName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Student> getStudent() {
		if (student == null) {
			student = new EObjectWithInverseResolvingEList<Student>(Student.class, this,
					CourseModelingPackage.STUDY_PROGRAMS__STUDENT, CourseModelingPackage.STUDENT__STUDYPROGRAMS);
		}
		return student;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourse() {
		if (course == null) {
			course = new EObjectContainmentEList<Course>(Course.class, this,
					CourseModelingPackage.STUDY_PROGRAMS__COURSE);
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			return ((InternalEList<InternalEObject>) (InternalEList<?>) getStudent()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			return ((InternalEList<?>) getStudent()).basicRemove(otherEnd, msgs);
		case CourseModelingPackage.STUDY_PROGRAMS__COURSE:
			return ((InternalEList<?>) getCourse()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_CODE:
			return getStudyProgramCode();
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_NAME:
			return getStudyProgramName();
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			return getStudent();
		case CourseModelingPackage.STUDY_PROGRAMS__COURSE:
			return getCourse();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_CODE:
			setStudyProgramCode((String) newValue);
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_NAME:
			setStudyProgramName((String) newValue);
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			getStudent().clear();
			getStudent().addAll((Collection<? extends Student>) newValue);
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__COURSE:
			getCourse().clear();
			getCourse().addAll((Collection<? extends Course>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_CODE:
			setStudyProgramCode(STUDY_PROGRAM_CODE_EDEFAULT);
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_NAME:
			setStudyProgramName(STUDY_PROGRAM_NAME_EDEFAULT);
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			getStudent().clear();
			return;
		case CourseModelingPackage.STUDY_PROGRAMS__COURSE:
			getCourse().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_CODE:
			return STUDY_PROGRAM_CODE_EDEFAULT == null ? studyProgramCode != null
					: !STUDY_PROGRAM_CODE_EDEFAULT.equals(studyProgramCode);
		case CourseModelingPackage.STUDY_PROGRAMS__STUDY_PROGRAM_NAME:
			return STUDY_PROGRAM_NAME_EDEFAULT == null ? studyProgramName != null
					: !STUDY_PROGRAM_NAME_EDEFAULT.equals(studyProgramName);
		case CourseModelingPackage.STUDY_PROGRAMS__STUDENT:
			return student != null && !student.isEmpty();
		case CourseModelingPackage.STUDY_PROGRAMS__COURSE:
			return course != null && !course.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (studyProgramCode: ");
		result.append(studyProgramCode);
		result.append(", studyProgramName: ");
		result.append(studyProgramName);
		result.append(')');
		return result.toString();
	}

} //StudyProgramsImpl
