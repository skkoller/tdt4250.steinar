/**
 */
package tdt4250.steinar.courseModeling.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentEList;
import org.eclipse.emf.ecore.util.InternalEList;

import tdt4250.steinar.courseModeling.Course;
import tdt4250.steinar.courseModeling.CourseModelingPackage;
import tdt4250.steinar.courseModeling.Department;
import tdt4250.steinar.courseModeling.Employee;
import tdt4250.steinar.courseModeling.StudyPrograms;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Department</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getDepartmentName <em>Department Name</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getPhoneNummber <em>Phone Nummber</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getEmail <em>Email</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getCourse <em>Course</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getEmployee <em>Employee</em>}</li>
 *   <li>{@link tdt4250.steinar.courseModeling.impl.DepartmentImpl#getStudyprograms <em>Studyprograms</em>}</li>
 * </ul>
 *
 * @generated
 */
public class DepartmentImpl extends MinimalEObjectImpl.Container implements Department {
	/**
	 * The default value of the '{@link #getDepartmentName() <em>Department Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepartmentName()
	 * @generated
	 * @ordered
	 */
	protected static final String DEPARTMENT_NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getDepartmentName() <em>Department Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getDepartmentName()
	 * @generated
	 * @ordered
	 */
	protected String departmentName = DEPARTMENT_NAME_EDEFAULT;

	/**
	 * The default value of the '{@link #getPhoneNummber() <em>Phone Nummber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhoneNummber()
	 * @generated
	 * @ordered
	 */
	protected static final String PHONE_NUMMBER_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getPhoneNummber() <em>Phone Nummber</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getPhoneNummber()
	 * @generated
	 * @ordered
	 */
	protected String phoneNummber = PHONE_NUMMBER_EDEFAULT;

	/**
	 * The default value of the '{@link #getEmail() <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmail()
	 * @generated
	 * @ordered
	 */
	protected static final String EMAIL_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getEmail() <em>Email</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmail()
	 * @generated
	 * @ordered
	 */
	protected String email = EMAIL_EDEFAULT;

	/**
	 * The cached value of the '{@link #getCourse() <em>Course</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getCourse()
	 * @generated
	 * @ordered
	 */
	protected EList<Course> course;

	/**
	 * The cached value of the '{@link #getEmployee() <em>Employee</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getEmployee()
	 * @generated
	 * @ordered
	 */
	protected EList<Employee> employee;

	/**
	 * The cached value of the '{@link #getStudyprograms() <em>Studyprograms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getStudyprograms()
	 * @generated
	 * @ordered
	 */
	protected EList<StudyPrograms> studyprograms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected DepartmentImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return CourseModelingPackage.Literals.DEPARTMENT;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getDepartmentName() {
		return departmentName;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setDepartmentName(String newDepartmentName) {
		String oldDepartmentName = departmentName;
		departmentName = newDepartmentName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseModelingPackage.DEPARTMENT__DEPARTMENT_NAME,
					oldDepartmentName, departmentName));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getPhoneNummber() {
		return phoneNummber;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setPhoneNummber(String newPhoneNummber) {
		String oldPhoneNummber = phoneNummber;
		phoneNummber = newPhoneNummber;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseModelingPackage.DEPARTMENT__PHONE_NUMMBER,
					oldPhoneNummber, phoneNummber));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setEmail(String newEmail) {
		String oldEmail = email;
		email = newEmail;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, CourseModelingPackage.DEPARTMENT__EMAIL, oldEmail,
					email));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Employee> getEmployee() {
		if (employee == null) {
			employee = new EObjectContainmentEList<Employee>(Employee.class, this,
					CourseModelingPackage.DEPARTMENT__EMPLOYEE);
		}
		return employee;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<StudyPrograms> getStudyprograms() {
		if (studyprograms == null) {
			studyprograms = new EObjectContainmentEList<StudyPrograms>(StudyPrograms.class, this,
					CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS);
		}
		return studyprograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Course> getCourse() {
		if (course == null) {
			course = new EObjectContainmentEList<Course>(Course.class, this, CourseModelingPackage.DEPARTMENT__COURSE);
		}
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
		case CourseModelingPackage.DEPARTMENT__COURSE:
			return ((InternalEList<?>) getCourse()).basicRemove(otherEnd, msgs);
		case CourseModelingPackage.DEPARTMENT__EMPLOYEE:
			return ((InternalEList<?>) getEmployee()).basicRemove(otherEnd, msgs);
		case CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS:
			return ((InternalEList<?>) getStudyprograms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case CourseModelingPackage.DEPARTMENT__DEPARTMENT_NAME:
			return getDepartmentName();
		case CourseModelingPackage.DEPARTMENT__PHONE_NUMMBER:
			return getPhoneNummber();
		case CourseModelingPackage.DEPARTMENT__EMAIL:
			return getEmail();
		case CourseModelingPackage.DEPARTMENT__COURSE:
			return getCourse();
		case CourseModelingPackage.DEPARTMENT__EMPLOYEE:
			return getEmployee();
		case CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS:
			return getStudyprograms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case CourseModelingPackage.DEPARTMENT__DEPARTMENT_NAME:
			setDepartmentName((String) newValue);
			return;
		case CourseModelingPackage.DEPARTMENT__PHONE_NUMMBER:
			setPhoneNummber((String) newValue);
			return;
		case CourseModelingPackage.DEPARTMENT__EMAIL:
			setEmail((String) newValue);
			return;
		case CourseModelingPackage.DEPARTMENT__COURSE:
			getCourse().clear();
			getCourse().addAll((Collection<? extends Course>) newValue);
			return;
		case CourseModelingPackage.DEPARTMENT__EMPLOYEE:
			getEmployee().clear();
			getEmployee().addAll((Collection<? extends Employee>) newValue);
			return;
		case CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS:
			getStudyprograms().clear();
			getStudyprograms().addAll((Collection<? extends StudyPrograms>) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case CourseModelingPackage.DEPARTMENT__DEPARTMENT_NAME:
			setDepartmentName(DEPARTMENT_NAME_EDEFAULT);
			return;
		case CourseModelingPackage.DEPARTMENT__PHONE_NUMMBER:
			setPhoneNummber(PHONE_NUMMBER_EDEFAULT);
			return;
		case CourseModelingPackage.DEPARTMENT__EMAIL:
			setEmail(EMAIL_EDEFAULT);
			return;
		case CourseModelingPackage.DEPARTMENT__COURSE:
			getCourse().clear();
			return;
		case CourseModelingPackage.DEPARTMENT__EMPLOYEE:
			getEmployee().clear();
			return;
		case CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS:
			getStudyprograms().clear();
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case CourseModelingPackage.DEPARTMENT__DEPARTMENT_NAME:
			return DEPARTMENT_NAME_EDEFAULT == null ? departmentName != null
					: !DEPARTMENT_NAME_EDEFAULT.equals(departmentName);
		case CourseModelingPackage.DEPARTMENT__PHONE_NUMMBER:
			return PHONE_NUMMBER_EDEFAULT == null ? phoneNummber != null : !PHONE_NUMMBER_EDEFAULT.equals(phoneNummber);
		case CourseModelingPackage.DEPARTMENT__EMAIL:
			return EMAIL_EDEFAULT == null ? email != null : !EMAIL_EDEFAULT.equals(email);
		case CourseModelingPackage.DEPARTMENT__COURSE:
			return course != null && !course.isEmpty();
		case CourseModelingPackage.DEPARTMENT__EMPLOYEE:
			return employee != null && !employee.isEmpty();
		case CourseModelingPackage.DEPARTMENT__STUDYPROGRAMS:
			return studyprograms != null && !studyprograms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String toString() {
		if (eIsProxy())
			return super.toString();

		StringBuilder result = new StringBuilder(super.toString());
		result.append(" (departmentName: ");
		result.append(departmentName);
		result.append(", phoneNummber: ");
		result.append(phoneNummber);
		result.append(", email: ");
		result.append(email);
		result.append(')');
		return result.toString();
	}

} //DepartmentImpl
