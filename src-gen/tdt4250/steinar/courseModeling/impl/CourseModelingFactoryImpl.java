/**
 */
package tdt4250.steinar.courseModeling.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import tdt4250.steinar.courseModeling.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class CourseModelingFactoryImpl extends EFactoryImpl implements CourseModelingFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static CourseModelingFactory init() {
		try {
			CourseModelingFactory theCourseModelingFactory = (CourseModelingFactory) EPackage.Registry.INSTANCE
					.getEFactory(CourseModelingPackage.eNS_URI);
			if (theCourseModelingFactory != null) {
				return theCourseModelingFactory;
			}
		} catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new CourseModelingFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseModelingFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
		case CourseModelingPackage.EMPLOYEE:
			return createEmployee();
		case CourseModelingPackage.STUDY_PROGRAMS:
			return createStudyPrograms();
		case CourseModelingPackage.COURSE_WORKER:
			return createCourseWorker();
		case CourseModelingPackage.COURSE_WORK_OBJECT:
			return createCourseWorkObject();
		case CourseModelingPackage.COURSE_WORK:
			return createCourseWork();
		case CourseModelingPackage.EVALUATIONS:
			return createEvaluations();
		case CourseModelingPackage.PRECONDITION:
			return createPrecondition();
		case CourseModelingPackage.PERSON:
			return createPerson();
		case CourseModelingPackage.REDUCTION:
			return createReduction();
		case CourseModelingPackage.COURSE:
			return createCourse();
		case CourseModelingPackage.COURSE_INSTANCE:
			return createCourseInstance();
		case CourseModelingPackage.EVALUATION_OBJECT:
			return createEvaluationObject();
		case CourseModelingPackage.DEPARTMENT:
			return createDepartment();
		case CourseModelingPackage.STUDENT:
			return createStudent();
		default:
			throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object createFromString(EDataType eDataType, String initialValue) {
		switch (eDataType.getClassifierID()) {
		case CourseModelingPackage.COURSE_WORK_TYPE:
			return createCourseWorkTypeFromString(eDataType, initialValue);
		case CourseModelingPackage.TERM_TYPE:
			return createTermTypeFromString(eDataType, initialValue);
		case CourseModelingPackage.EVALUATION_TYPE:
			return createEvaluationTypeFromString(eDataType, initialValue);
		case CourseModelingPackage.PERSON_ROLE_TYPE:
			return createpersonRoleTypeFromString(eDataType, initialValue);
		case CourseModelingPackage.PRECONDISTION_TYPE:
			return createPrecondistionTypeFromString(eDataType, initialValue);
		case CourseModelingPackage.DAY:
			return createDayFromString(eDataType, initialValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public String convertToString(EDataType eDataType, Object instanceValue) {
		switch (eDataType.getClassifierID()) {
		case CourseModelingPackage.COURSE_WORK_TYPE:
			return convertCourseWorkTypeToString(eDataType, instanceValue);
		case CourseModelingPackage.TERM_TYPE:
			return convertTermTypeToString(eDataType, instanceValue);
		case CourseModelingPackage.EVALUATION_TYPE:
			return convertEvaluationTypeToString(eDataType, instanceValue);
		case CourseModelingPackage.PERSON_ROLE_TYPE:
			return convertpersonRoleTypeToString(eDataType, instanceValue);
		case CourseModelingPackage.PRECONDISTION_TYPE:
			return convertPrecondistionTypeToString(eDataType, instanceValue);
		case CourseModelingPackage.DAY:
			return convertDayToString(eDataType, instanceValue);
		default:
			throw new IllegalArgumentException("The datatype '" + eDataType.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Employee createEmployee() {
		EmployeeImpl employee = new EmployeeImpl();
		return employee;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public StudyPrograms createStudyPrograms() {
		StudyProgramsImpl studyPrograms = new StudyProgramsImpl();
		return studyPrograms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWorker createCourseWorker() {
		CourseWorkerImpl courseWorker = new CourseWorkerImpl();
		return courseWorker;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWorkObject createCourseWorkObject() {
		CourseWorkObjectImpl courseWorkObject = new CourseWorkObjectImpl();
		return courseWorkObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWork createCourseWork() {
		CourseWorkImpl courseWork = new CourseWorkImpl();
		return courseWork;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Evaluations createEvaluations() {
		EvaluationsImpl evaluations = new EvaluationsImpl();
		return evaluations;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Precondition createPrecondition() {
		PreconditionImpl precondition = new PreconditionImpl();
		return precondition;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Person createPerson() {
		PersonImpl person = new PersonImpl();
		return person;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Reduction createReduction() {
		ReductionImpl reduction = new ReductionImpl();
		return reduction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Course createCourse() {
		CourseImpl course = new CourseImpl();
		return course;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseInstance createCourseInstance() {
		CourseInstanceImpl courseInstance = new CourseInstanceImpl();
		return courseInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationObject createEvaluationObject() {
		EvaluationObjectImpl evaluationObject = new EvaluationObjectImpl();
		return evaluationObject;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Department createDepartment() {
		DepartmentImpl department = new DepartmentImpl();
		return department;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Student createStudent() {
		StudentImpl student = new StudentImpl();
		return student;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseWorkType createCourseWorkTypeFromString(EDataType eDataType, String initialValue) {
		CourseWorkType result = CourseWorkType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertCourseWorkTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public TermType createTermTypeFromString(EDataType eDataType, String initialValue) {
		TermType result = TermType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertTermTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EvaluationType createEvaluationTypeFromString(EDataType eDataType, String initialValue) {
		EvaluationType result = EvaluationType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertEvaluationTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public personRoleType createpersonRoleTypeFromString(EDataType eDataType, String initialValue) {
		personRoleType result = personRoleType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertpersonRoleTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrecondistionType createPrecondistionTypeFromString(EDataType eDataType, String initialValue) {
		PrecondistionType result = PrecondistionType.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertPrecondistionTypeToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Day createDayFromString(EDataType eDataType, String initialValue) {
		Day result = Day.get(initialValue);
		if (result == null)
			throw new IllegalArgumentException(
					"The value '" + initialValue + "' is not a valid enumerator of '" + eDataType.getName() + "'");
		return result;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String convertDayToString(EDataType eDataType, Object instanceValue) {
		return instanceValue == null ? null : instanceValue.toString();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public CourseModelingPackage getCourseModelingPackage() {
		return (CourseModelingPackage) getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static CourseModelingPackage getPackage() {
		return CourseModelingPackage.eINSTANCE;
	}

} //CourseModelingFactoryImpl
