Task 2 Transformation:

Introduction: 
The tranformation is a M2T transformation based on the model described below. It takes in an depart model that consist of studyprograms, courses and course instances and generate a simple HTML page.

Implementation:
src-gen folder contains the code generated by the model
src folder contains the manualy written code for the transformation, it is a simple version where CoursePageGenerator.java takes in an uri and CoursePageHTMLGenerator.java generates HTML pages from the uri.

Required features/plugins:
org.eclipse.emf.ecore;visibility:=reexport,
org.eclipse.core.runtime,
org.eclipse.emf.ecore.xmi;bundle-version="2.14.0"

Running the program:
To run the tranformation navigate to the CoursePageGenerator.java in teh src folder. You can either run it as an java application, then it will take in model/Department.xmi and generate html pages in the model folder. Or you can select run configuration and provide your own uri as specified in the excercise description

Task 1 Model:

Classes

Course: 
     
Contains CourseInstance objects, and has standard information about a course

CourseInstance: 
     
Contains CourseWork and a Evaluations object. Holds information specific to a specific semester

Person: 
     
Superclass for Employee and Student. Holds the persons name and other information

Employee: 
     
A person who works for NTNU

Student: 
     
A person who studies at NTNU

CourseWork: 
     
Contains CourseWorkObjects

CourseWorkObjects: 
     
A class holding information about when and where the course work takes place

Department: 
     
Part of the school that manages various course instances

StudyProgram: 
     
A line of study students get registered to. Provides a collection of courses

Evaluations: 
     
Contains EvaluationObjects, makes sure credit sum is 100

EvaluationObjects: 
     
Contains information about type of evaluation and credits.
